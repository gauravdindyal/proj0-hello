# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

Author: Gaurav Dindyal

email id: gauravd@uoregon.edu

bitbucket url for project0: https://bitbucket.org/gauravdindyal/proj0-hello/

CIS 322 FALL2020

Description: “Basic Python program that prints “Hello World””

